"""Test the random generated ibans list."""

import pytest
from schwifty import IBAN
from schwifty import exceptions as s_except

from tests.random_iban import (
    TEST_IBAN,
    invalidate_iban,
    random_iban,
    random_invalid_iban,
)


def test_iban_validity() -> None:
    """Test validitiy of the IBANs by Schwifty library."""
    result = []

    for iban in TEST_IBAN:
        result.append(IBAN(iban))

    for result_object in result:
        assert isinstance(result_object, IBAN)


def test_random_iban() -> None:
    """Test getting a random iban in the list."""
    result = set()

    for _ in range(20):  # noqa: WPS432
        result.add(random_iban())

    assert len(result) > 1


def test_random_invalid_iban_and_invalidate_iban() -> None:
    """Test getting a random iban in the list."""
    result = set()

    for _ in range(20):  # noqa: WPS432
        result.add(random_invalid_iban())

    assert len(result) > 1


def test_invalidate_iban() -> None:
    """Test making IBAN invalid."""
    result = []

    for iban in TEST_IBAN:
        with pytest.raises(Exception) as exception:  # noqa: PT011
            IBAN(invalidate_iban(iban))
        result.append(exception)  # noqa: WPS441

    for result_object in result:
        assert type(result_object.value) in {  # noqa: WPS516
            s_except.InvalidAccountCode,
            s_except.InvalidChecksumDigits,
            s_except.InvalidCountryCode,
            s_except.InvalidLength,
        }
