"""Test insert functionality of SQL models."""

from datetime import datetime
from typing import List

import pytest
from rpycihelpers.foldered_test import FolderedPyTest  # type: ignore
from sqlmodel import SQLModel

from cliba.database.engine import create_init_engine
from cliba.database.insert import insert
from cliba.database.schema.transaction import Transaction
from tests.random_iban import random_iban


class InsertTest(FolderedPyTest):
    """Test insertion."""

    @pytest.mark.filterwarnings("ignore:sqlalchemy.sql.type_api")
    @staticmethod
    def test_insert_ignore_existing() -> None:
        """Test inserting two equal objects."""
        iban_account = random_iban()
        iban_applicant = random_iban()
        obj_dict = dict(  # noqa: C408
            account_iban=iban_account,
            applicant_iban=iban_applicant,
            date=datetime.now(),
            currency="EUR",
            amount=123.34,  # noqa: WPS432
            raw_data={"a": "test"},
            purpose="Test",
        )
        engine = create_init_engine("sqlite:///test.db")
        result = None
        test_transactions: List[SQLModel] = [
            Transaction(**obj_dict),
            Transaction(**obj_dict),
        ]

        try:
            insert(engine, test_transactions)
        except (Exception) as exception:  # noqa: WPS313
            result = exception

        assert result is None
