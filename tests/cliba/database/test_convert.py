"""Test conversion."""

import datetime as dt
from decimal import Decimal

from fints.models import SEPAAccount  # type: ignore
from mt940.models import Amount as mtAmount  # type: ignore
from mt940.models import Balance as mtBalance  # type: ignore
from mt940.models import Transaction as mtTransaction  # type: ignore

from cliba.database import convert
from cliba.database.schema.balance import Balance
from cliba.database.schema.transaction import Transaction
from tests.random_iban import random_iban


def create_test_account(iban: str = None) -> SEPAAccount:
    """Create an SEPAAccount for testing.

    :param iban: A fixed iban number
    :returns: SEPAAccount class for testing
    """
    return SEPAAccount(
        iban=iban or random_iban(),
        # Required fields for this class
        # cliba does not use - they are not validated.
        bic="doesnotmatter",
        accountnumber="1234567",
        subaccount=None,
        blz="1234567",
    )


def create_test_amount(amount: str = "123.45", currency: str = "EUR") -> mtAmount:
    """Create a test amount object for testing.

    :param amount: String for the amount of currency
    :param currency: Currency string like EUR, USD ...
    :returns: A mtAmount of 123.45 EUR
    """
    return mtAmount(amount=amount, currency=currency, status="C")


def test_convert_to_database_balance() -> None:
    """Test converting balance."""
    value = "12345.45"
    currency = "EUR"
    iban = random_iban()
    test_account = create_test_account(iban)
    test_balance = mtBalance(amount=create_test_amount(value, currency), status="C")

    result = convert.to_database_balance(test_account, test_balance)

    assert isinstance(result, Balance)
    assert result.amount == Decimal(value)
    assert result.currency == currency
    assert result.account_iban == iban


def test_convert_to_database_transaction() -> None:
    """Test converting balance."""
    transfer_amount = "12345.45"
    currency = "EUR"
    iban_account = random_iban()
    iban_transaction = random_iban()
    test_account = create_test_account(iban_account)
    test_purpose = "test_purpose"
    test_date = dt.date.today()  # noqa: WPS432
    test_transaction = mtTransaction(
        [],
        dict(  # noqa: C408
            applicant_iban=iban_transaction,
            amount=create_test_amount(transfer_amount, currency),
            date=test_date,
            currency=currency,
            purpose=test_purpose,
        ),
    )

    result = convert.to_database_transaction(test_account, test_transaction)

    assert isinstance(result, Transaction)
    assert result == Transaction(
        amount=Decimal(transfer_amount),
        currency=currency,
        purpose=test_purpose,
        applicant_iban=iban_transaction,
        account_iban=iban_account,
        date=result.date,
        raw_data=result.raw_data,
    )
