"""Test transaction model."""

from datetime import datetime

import pytest
from rpycihelpers.foldered_test import FolderedPyTest  # type: ignore
from sqlalchemy.exc import IntegrityError, SQLAlchemyError
from sqlmodel import Session

from cliba.database.engine import create_init_engine
from cliba.database.schema.transaction import Transaction
from tests.random_iban import random_iban


class TransactionTest(FolderedPyTest):
    """Test Transaction object model."""

    @staticmethod
    def test_creating_transaction_good_iban() -> None:
        """Test creating transaction."""
        iban_account = random_iban()
        iban_applicant = random_iban()

        result = Transaction(
            account_iban=iban_account,
            applicant_iban=iban_applicant,
            date=datetime.now(),
            currency="EUR",
            amount=123.34,  # noqa: WPS432
            raw_data={"a": "test"},
        )

        assert isinstance(result, Transaction)

    @staticmethod
    def test_inserting_duplicated_transaction() -> None:
        """Test to insert same transaction twice."""
        iban_account = random_iban()
        iban_applicant = random_iban()
        obj_dict = dict(  # noqa: C408
            account_iban=iban_account,
            applicant_iban=iban_applicant,
            date=datetime.now(),
            currency="EUR",
            amount=123.34,  # noqa: WPS432
            raw_data={"a": "test"},
            purpose="Test",
        )

        engine = create_init_engine("sqlite:///test.db")

        with Session(engine) as session:
            session.add(Transaction(**obj_dict))
            session.add(Transaction(**obj_dict))
            with pytest.raises(SQLAlchemyError) as exception:
                session.commit()
        result = exception  # noqa: WPS441

        assert result.errisinstance(IntegrityError)
