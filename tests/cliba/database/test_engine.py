"""Test creating the database and engine."""

import os

from rpycihelpers.foldered_test import FolderedPyTest  # type: ignore
from sqlalchemy.future.engine import Engine

from cliba.database.engine import create_init_engine


class EngineTest(FolderedPyTest):
    """Database creation tests."""

    @staticmethod
    def test_create_init_engine() -> None:
        """Test creating the database."""
        engine = create_init_engine("sqlite:///test.db")

        assert isinstance(engine, Engine)
        assert os.path.isfile("test.db")
