"""Test using config file type."""

import yaml

from cliba.config.config_file import ConfigFile, FinTSAccount


def test_fints_account() -> None:
    """Test generate FinTS account from dict."""
    obj_dict = dict(  # noqa: C408
        user_name="user",
        bank_identifier="bank_id",
        server_url="sqlite:///",
        customer_id="123456",
        pin="1234",
    )

    result = FinTSAccount(**obj_dict)

    assert isinstance(result, FinTSAccount)


def test_config_file() -> None:
    """Test constructing config file from dict."""
    obj_def = """
    accounts:
      "Eisen..Reifen":
         user_name: test_user
         bank_identifier: 123456
         server_url: https://eisenreifen.de
      SpassKass:
         user_name: test_user
         bank_identifier: ABC
         server_url: https://spass.kass
         customer_id: MyID
         pin: PasswordYouShouldProbablyNotStoreHere
    debug_level: warning
    database_url: "sqlite:///"
    """
    obj_dict = yaml.safe_load(obj_def)

    result = ConfigFile(**obj_dict)

    assert isinstance(result, ConfigFile)
    assert len(result.accounts) == 2
    assert result.debug_level == "warning"
    assert result.database_url == "sqlite:///"
