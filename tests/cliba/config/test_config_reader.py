"""Test config reader."""

from unittest.mock import patch

import yaml
from rpycihelpers.files import create_file  # type: ignore
from rpycihelpers.foldered_test import FolderedPyTest  # type: ignore

from cliba.config.config_file import ConfigFile
from cliba.config.config_reader import find_config_file, read_config, write_config


class ConfigReaderTest(FolderedPyTest):
    """Test insertion."""

    @staticmethod
    def test_write_config_file() -> None:
        """Test writing config file."""
        config_content = ConfigFile()
        config_path = "test.yaml"
        result = {}

        write_config(config_path, config_content)

        with open(config_path, "r") as test_file:
            result = yaml.safe_load(test_file.read())

        assert isinstance(result, dict)
        assert result.get("accounts") == {}  # noqa: WPS520
        assert result.get("debug_level") == "info"
        assert result.get("database_url") is None

    @staticmethod
    def test_read_config() -> None:
        """Test reading config file."""
        config_txt = """
        accounts:
          test:
            user_name: user
            bank_identifier: ident123
            server_url: http://fints.bank.com
            customer_id: customer_id123
            pin: NOTRECOMMENDED.
        debug_level: info
        database_url: sqlite://my.db
        """
        config_path = "test.yaml"
        create_file(config_path, config_txt)

        result = read_config(config_path)

        assert isinstance(result, ConfigFile)
        assert len(result.accounts) == 1
        assert result.debug_level == "info"
        assert result.database_url

    @staticmethod
    def test_find_config_file() -> None:
        """Test find config file."""
        config_path = "test.yaml"
        create_file(config_path, "does not need to be valid here.")
        with patch(
            "cliba.config.config_reader.config_locations", ["notthere.yaml", config_path]
        ):
            result = find_config_file()

        assert result == config_path
