"""Randomized IBAN numbers.

These numbers were created randomized by an online number generator
(to name it: http://www.randomiban.com).

Any match with real existing IBANs is unintentional!
So if you happen to find your own IBAN in here and want it removed,
just tell me.
"""

import random
import string

TEST_IBAN = (
    "NL17INGB4054767621",
    "FR6910096000303226465378L80",
    "DE06500105171576769716",
    "DE73500105175968437593",
    "GB07BARC20037888444485",
    "LI4308800868189927337",
    "IS788127817963321578449742",
    "BR4193945514477299398743588L5",
    "DE77500105175588139418",
    "DE24500105172541273275",
    "IS307989173186923744115828",
    "IT02O0300203280895584846169",
    "ES8900811179269918232449",
    "SE0374438467329253959671",
    "GR1001196271681743115983776",
    "MK65319256652393393",
    "HU02131000078491243437444624",
    "DK9850517695323564",
    "GL8972711179499851",
    "BE43548762494201",
    "CY03245428644772145181246112",
)


def invalidate_iban(iban: str) -> str:
    """Add a error to the iban at a random position.

    :param iban: The IBAN
    :returns: Invalid IBAN with one manipulated character.
    """
    iban = iban.upper()
    place = random.randint(0, len(iban) - 1)  # noqa: S311
    char = iban[place]

    replacement_chars = string.ascii_uppercase.replace(char, "")
    if char in string.digits:
        replacement_chars = string.digits.replace(char, "")

    char = random.choice(replacement_chars)  # noqa: S311

    return iban[:place] + char + iban[place + 1 :]


def random_iban() -> str:
    """Pick a random iban from the list.

    :returns: A random iban from the test iban list.
    """
    return random.choice(TEST_IBAN)  # noqa: S311


def random_invalid_iban() -> str:
    """Pick a random iban from the list.

    :returns: A random iban from the test iban list.
    """
    return invalidate_iban(random.choice(TEST_IBAN))  # noqa: S311
