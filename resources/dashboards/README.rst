Grafana Dashboards
==================

The currency used over all dashboards is Euro.
Unfortunately there was no obvious way to set this with
a query. To change the currency just replace the string
"currencyEUR" by the search and replace
feature of a text editor.

The database used for the dashboards is PostgreSQL.

Home
----

File: home.json

Variables:
^^^^^^^^^^

- account_database

  The database used for the dashboard.


Panels:
^^^^^^^

- Account balances

  The balances of all accounts in the database,
  each dashboard gets its own gauge

- Dashboards

  The panel shows all system dashboards by search and
  starred ones.

- Earnings

  The earnings graph shows sactions grouped
  by account IBAN and shows a bar for all incoming
  transactions summed up over each month.

- Spendings

  The spending graph shows all transactions grouped
  by account IBAN and shows a bar for all outgoing
  transactions summed up over each month.

- Balance over Time

  The graph shows all balances over the currently
  selected timespan and grouped by IBAN.
