CLIBa(nk Synchronizer)
===============================

This code uses FinTS library to synchronize one or multiple
FinTS enpoints to a local database for use with tools like Grafana.

.. image:: img/example_panel1.png
  :width: 923
  :alt: Example Grafana Panel

Usage
=====

Cliba also has a buildin help.

.. code-block:: shell

   cliba --help


Code Completion
---------------

Due to the usage of Typer it also features code completion.
Completion can be installed by:

.. code-block:: shell

   cliba --install-completion

.. note::

   You might need to relogin to the shell to enable the completion.


Settings File
-------------

The settings file can normally be found in the user directory.

To output it to the command line use:

.. code-block:: shell

   cat /home/$USER/.cliba.yaml

This file can also store the PIN to each FinTS account.
This is not supported by the CLI tool (due to the security issues it brings).

.. warning::
   Storing the PIN inside a file as clear text imposes a security risk!


Create FinTS account
--------------------

A FinTS account is not related to the actual bank account.
The FinTS account is the login information used to access your
user account of your bank. This account can have multiple bank accounts.

CLIBa currently supports Balances and SEPAAccounts.

It does not support transactions.

To create an account use the following command.

.. code-block:: shell

   cliba add-account [OPTIONS] ALIAS BANK_IDENTIFIER USER_ID SERVER_URL

Arguments
^^^^^^^^^

- ALIAS

  The name you referer to this account later on.
  Its recommended to not use any special characters and replace whitespaces with underscores.

- BANK_IDENTIFIER

  This is the bank identifier. A FinTS URL can feature access to multiple banks.
  It can be for example the BIN of the bank.

- USER_ID

  This is the user id for the accout.

- SERVER_URL

  This is the actual URL, ask your bank for it.

Options
^^^^^^^

- customer_id  (--customer-id)
  This value is optional for a bank. It depends on your bank if you need it.

- force (--force)
  As an alias is unique you can overwrite the current configuration by force.


Set database connection
-----------------------

Cliba needs to use a database to store the balances and transaction values.
This is necessary to access them with tools like Grafana.
Also most banks delete the machine readable records after some months,
so storing them on your side gives you a bigger overview.

As CLIBa uses SQLModel which is based on SQLAlchemy, you are free to choose
from a variety of databases.
See `supported dialects <https://docs.sqlalchemy.org/en/14/dialects/>`_

.. note::
   Dashboards in the resources folder use PostgreSQL.
   Tool Tests use SQLite for the reason of not needing a database server.

.. warning::
   SQLite does not feature numeric values (used for transfer/balance amount),
   they will be converted to float, this might lead to value errors.


The database url can be set like in the following examples.

.. code-block:: shell

   # Example for SQLite (File)
   cliba setting database "sqlite:////home/$USER/my_bank_data.yaml"

   # Example for PostgreSQL using python module pg8000
   # See resources/compose_psql_db for a docker-compose setup with postgres
   cliba setting database "postgresql+pg8000://user:password@localhost:5432/bank"


Sync balances/accounts to database
----------------------------------

To sync all SEPA accounts of an FinTS account call the following command.

.. code-block:: shell

   cliba sync ALIAS

After executing that command, you might be queried for PIN and TAN numbers, as
unfortunately the TAN is needed for downloading transfer data too.

Arguments
^^^^^^^^^

- ALIAS

  The name you referer to this account later on.
  Its recommended to not use any special characters and replace whitespaces with underscores.

Environment Variables
^^^^^^^^^^^^^^^^^^^^^

- CLIBA_PIN_<ALIAS>

  This environment variable can be used to set the PIN in your shell environment.
  Assuming your ALIAS is "my_bank_alias" then the value can be set like:

.. code-block:: shell

   export CLIBA_PIN_MY_BANK_ALIAS="MYPIN"


Development
===========

In the project directory (where the pyproject.toml resides):

Install development environment:

.. code-block:: shell

   poetry install


Enable pre-commit hooks:

.. code-block:: shell

   pre-commit install

Change to the python environment:

.. code-block:: shell

   poetry shell



Linting and Code Formatting with Black
======================================

Disable linter errors:

Create a noqa comment as seen in this example.

.. code-block:: python

    result = subprocess.run(  # noqa: S603,S607
        [test_command],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,  # noqa: S602
        cwd=bake_result.project_path,
    )



Disable formatting of black for a code block

.. code-block:: python

   # fmt: off

   var = [
     10, 10, 10,
     20, 20, 20
   ]

   # fmt: on
