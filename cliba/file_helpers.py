"""Implement some simple file helpers."""

import os
from typing import List


def list_sub_dirs(directory: str) -> List[str]:
    """List all subdirectories of the given directory.

    :param directory: The directory to list the subdirectories
    :returns: List of subdirectories.
    """
    subdirs = []

    for path in os.listdir(directory):
        if os.path.isdir(path):
            subdirs.append(path)

    return subdirs


def create_dir(*directory: str) -> None:
    """Create directory.

    :param directory: List of recursive subdirectories
    :raises RuntimeError: If path exists but is no directory.
    """
    joined_directory = os.path.join(*directory)

    if os.path.exists(joined_directory):
        if not os.path.isdir(joined_directory):
            raise RuntimeError(
                "Path for CLIBa directory exists, " f"but isn't a directory: {directory}"
            )
        return
    os.makedirs(joined_directory)
