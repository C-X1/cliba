"""Implement Fints client."""


import getpass
import os
import re
from typing import Any, Dict

from fints.client import FinTS3PinTanClient  # type: ignore

CLIENT_STATE_FILE = "client_state"


def ask_pin(kvargs: Dict[str, Any]) -> None:
    """Get pin from different sources.

    :param kvargs: The args for the FinTS Client
    """
    pin = kvargs["pin"]
    alias_env = re.sub("[- ].", "_", "").upper()
    # fmt: off
    pin = (
        pin
        or os.environ.get(f"CLIBA_PIN_{alias_env}")
        or getpass.getpass("PIN:")
    )
    # fmt: on
    kvargs["pin"] = pin


def create_fints_client(alias: str, **kvargs: Dict[str, Any]) -> FinTS3PinTanClient:
    """Create fints client.

    :param alias: The alias from the config
    :param **kvargs: The arguments for the FinTS Client
    :returns: Client
    """
    ask_pin(kvargs)
    kvargs["product_id"] = None  # type: ignore
    return FinTS3PinTanClient(**kvargs)
