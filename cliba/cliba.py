"""Construct and run main application."""

import logging
import sys

import structlog
import typer

###
# Typer imports "used" by @app.command() decorator in the file
###
from cliba.cli.add_account import add_account  # noqa: F401
from cliba.cli.app import app
from cliba.cli.migrate import export_db_table, import_transactions  # noqa: F401
from cliba.cli.remove_account import remove_account  # noqa: F401
from cliba.cli.setting import setting  # noqa: F401
from cliba.cli.sync import sync_accounts  # noqa: F401


@app.callback(no_args_is_help=True)
def callback() -> None:
    """Show help when called with no arguments."""


def main() -> None:
    """Construct typer_click interface."""
    log = structlog.get_logger()

    structlog.configure(
        wrapper_class=structlog.make_filtering_bound_logger(logging.DEBUG)
    )

    typer_click = typer.main.get_command(app)
    try:
        typer_click()
    except Exception as exception:
        log.fatal("Caught unhandled exception")
        log.exception(exception)
        sys.exit(1)


if __name__ == "__main__":  # pragma: no cover
    main()
