"""Implement tan guard."""

from typing import Any, Dict

from fints.client import NeedTANResponse  # type: ignore


def guard_tan_request(function: Any, **args: Dict[str, Any]) -> Any:  # noqa: ANN401
    """Guard a call to a tan request.

    :param function: The function to be called e.g. client.get_accounts
    :param **args: The arguments for the function.
    :returns: The actual command response
    """
    response = function(**args)
    while isinstance(response, NeedTANResponse):
        tan = prompt_tan(response)
        response = function.__self__.send_tan(response, tan)

    return response


def prompt_tan(request: NeedTANResponse) -> str:  # noqa: ANN401
    """Handle the tan request.

    :param request: TAN Request
    :returns: TAN.
    """
    challenge = request.challenge.replace("<br>", "\n")
    return input(f"{challenge}\n:: ")  # noqa: WPS421
