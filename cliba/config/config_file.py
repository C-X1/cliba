"""Specify config file."""

from typing import Dict, Literal, Union

from pydantic import BaseModel


class FinTSAccount(BaseModel):
    """Specify FinTS account."""

    user_id: str
    bank_identifier: str
    server: str
    customer_id: Union[str, None]
    pin: Union[str, None]


class ConfigFile(BaseModel):
    """Specify config file base."""

    accounts: Dict[str, FinTSAccount] = {}
    debug_level: Literal[
        "notset",
        "debug",
        "info",
        "warning",
        "error",
        "critical",
        "fatal",
    ] = "info"
    database_url: Union[str, None] = None
