"""Read the config file."""

import os
import sys
from typing import Union

import structlog
import yaml

from cliba.config.config_file import ConfigFile, FinTSAccount

HOME_DIR = os.environ.get("HOME")
DEBUG_LEVELS = ConfigFile.schema()["properties"]["debug_level"]["enum"]

logger = structlog.get_logger()
config_default_path = f"{HOME_DIR}/.cliba.yaml"
config_locations = [os.environ.get("CLIBA_CONFIG_PATH"), config_default_path]


def find_config_file() -> Union[str, None]:
    """Find the configuration file.

    :returns: Path of config or None
    """
    for config_location in config_locations:
        if config_location and os.path.isfile(config_location):
            return config_location

    return None


def read_config(path: Union[str, None] = None) -> ConfigFile:
    """Read config file.

    :param path: Overwrite config file path
    :returns: Config file content.
    """
    if path is None or not os.path.exists(path):
        return ConfigFile()

    if not os.path.isfile(path):
        logger.warn(f"Config File invalid: {path}")
        sys.exit(1)

    with open(path, "r") as config_file:
        config_file_dict = yaml.safe_load(config_file.read())
        return ConfigFile(**config_file_dict)


def write_config(path: str, config: ConfigFile) -> None:
    """Write config to file.

    :param path: Path to the file to write
    :param config: The config file object
    """
    with open(path, "w") as config_file:
        config_file.write(yaml.dump(config.dict()))


def add_account(  # noqa: WPS211
    alias: str,
    bank_identifier: str,
    user_id: str,
    server: str,
    customer_id: str,
    force: bool,
) -> None:
    """Add FinTS account.

    :param alias: The alias for the account
    :param bank_identifier: The identifier of the bank
    :param customer_id: The customer id (optional)
    :param user_id: The username you got from your bank.
    :param server: The server url for the fints endpoint.
    :param force: Force overwriting existing entries
                       Example:
                          https://hbci-pintan.gad.de/cgi-bin/hbciservlet (VR Banks)
    :raises KeyError: if alias exists and force is False
    """
    config_path = find_config_file()
    config_path = config_path or config_default_path
    config = read_config(config_path)

    if alias in config.accounts and not force:
        raise KeyError(f"Alias {alias} exists.")

    config.accounts[alias] = FinTSAccount(
        bank_identifier=bank_identifier,
        user_id=user_id,
        server=server,
        customer_id=customer_id,
    )

    write_config(config_path, config)
    logger.info(f"Set account {alias}")


def remove_account(alias: str) -> None:
    """Remove FinTS account from config.

    :param alias: The alias in the config
    :raises KeyError: If the alias is not in the config
    :returns: True on success, False if does not exist
    """
    config_path = find_config_file() or config_default_path
    config = read_config(config_path)

    if alias in config.accounts:
        config.accounts.pop(alias)
        write_config(config_path, config)
        logger.info(f"Removed account with alias {alias}")
        return

    raise KeyError(f"Alias {alias} does not exist.")


def set_database(url: str) -> None:
    """Set database in config.

    :param url: The database url
    """
    config_path = find_config_file() or config_default_path
    config = read_config(config_path)
    config.database_url = url
    write_config(config_path, config)


def set_debug_level(debug_level: str) -> None:
    """Set database in config.

    :raises ValueError: If debug_level is not existing.
    :param debug_level: The database url
    """
    if debug_level not in DEBUG_LEVELS:
        raise ValueError(f"Debug level {debug_level} does not exist.")

    config_path = find_config_file() or config_default_path
    config = read_config(config_path)
    config.debug_level = debug_level  # type: ignore
    write_config(config_path, config)
