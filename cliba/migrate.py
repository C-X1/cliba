"""Extract data from old database.

This is just a short quick script to extract my transaction table
of my old database to a yaml file.
"""

from typing import Any, Dict, List

import structlog
import yaml
from pydantic import ValidationError
from sqlalchemy import MetaData, Table
from sqlalchemy.engine import Engine
from sqlmodel import Session

from cliba.database.schema.transaction import Transaction

logger = structlog.getLogger()


def dict_rows_from_cursor(cursor: Any) -> List[Dict[str, Any]]:  # noqa: ANN401
    """Convert tuple result to dict with cursor.

    :param cursor: The cursor for the database, after execution.
    :returns: Array with dictionaries.
    """
    col_names = [column_name[0] for column_name in cursor.description]
    db_content = []
    for row in cursor:
        db_content.append(dict(zip(col_names, row)))

    return db_content


def stringify_table_entries(rows: List[Dict[str, Any]]) -> None:
    """Convert row entries to pure strings.

    :param rows: List with rows from the database
    """
    for row in rows:
        for col in row.keys():
            row[col] = str(row[col])


def export_table_to_yaml(
    engine: Engine,
    table_name: str,
    out_path: str,
) -> None:
    """Export database table.

    :param engine: SQLAlchemy engine for the database
    :param table_name: Name of the table
    :param out_path: Path of output file
    """
    table = Table(table_name, MetaData(), autoload_with=engine)

    cursor = table.select()
    db_content = dict_rows_from_cursor(cursor)
    stringify_table_entries(db_content)

    with open(out_path, "w") as yaml_file:
        yaml_file.write(yaml.dump(db_content))


def create_transactions_from_dicts(
    rows: List[Dict[str, str]], overwrites: Dict[str, str]
) -> List[Transaction]:
    """Create Transactions from dicts.

    :param rows: List of dictionaries to create the list from
    :param overwrites: List of overwrites to be done in each dictionary.
    :returns: List of transactions
    """
    transactions: List[Transaction] = []
    for row in rows:
        for overwrite in overwrites.keys():
            row[overwrite] = overwrites[overwrite]
        try:
            transaction = Transaction(**row)
        except ValidationError as validation_error:
            logger.error(f"Could not create Transaction for {row}")
            logger.error(str(validation_error))
            continue
        transaction.raw_data = row
        transactions.append(transaction)

    return transactions


def import_transactions_from_yaml(  # noqa: WPS210
    engine: Engine, data_path: str, overwrites: Dict[str, str]
) -> None:
    """Export database table.

    :param engine: SQLAlchemy engine for the database
    :param data_path: Path of input file
    :param overwrites: List of overwrites for every transaction field.
    """
    with open(data_path) as data_file:
        rows = yaml.safe_load(data_file.read())
        number_of_added_transactions = 0
        existing_datasets = 0
        transactions = create_transactions_from_dicts(rows, overwrites)

        for transaction in transactions:
            error = None
            with Session(engine) as session:
                session.add(transaction)
                try:
                    session.commit()
                except Exception as exception:
                    error = exception

                if error and "UniqueTransaction" in str(error):
                    existing_datasets += 1
                elif error:
                    logger.exception(error)
                    logger.error(f"Could not add Transaction: {transaction}")
                    session.rollback()

                if error is None:
                    number_of_added_transactions += 1

        logger.info(
            "Added {} of {} datasets! {} existed already.".format(
                number_of_added_transactions, len(rows), existing_datasets
            )
        )
