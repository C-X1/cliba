"""Remove account."""

import cliba
from cliba.cli.app import app
from cliba.cli.args import Arguments


@app.command()
def remove_account(alias: str = Arguments.alias) -> None:
    """Remove FinTS account.

    :param alias: The alias in the config
    """
    cliba.config.config_reader.remove_account(alias)
