"""Synchronize accounts to the database."""

from cliba.cli.app import app
from cliba.cli.args import Arguments
from cliba.sync_accounts import sync_accounts


@app.command()
def sync(alias: str = Arguments.alias) -> None:
    """Synchronize bank accounts with the database.

    :param alias: Alias of the account
    """
    sync_accounts(alias)
