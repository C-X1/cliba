"""Initialize Database."""

from typing import List

import typer

from cliba.cli.app import app
from cliba.config import config_reader

DATABASE = "database"
DEBUG_LEVEL = "debug_level"


def complete_setting() -> List[str]:
    """Complete possible settings.

    :returns: Possible settings
    """
    return [DATABASE, DEBUG_LEVEL]


def complete_value(ctx: typer.Context, incomplete: str) -> List[str]:
    """Complete values.

    :param incomplete: Incomplete string
    :param ctx: Typer context to get previous argument values
    :returns: Possible values.
    """
    option = ctx.params.get("option")

    if option == DEBUG_LEVEL:
        return config_reader.DEBUG_LEVELS

    return []


ARGUMENT_SETTING = typer.Argument(
    ...,
    help="The settings name",
    autocompletion=complete_setting,
)

VALUE_SETTING = typer.Argument(
    ..., help="The settings value", autocompletion=complete_value
)


@app.command()
def setting(
    option: str = ARGUMENT_SETTING,
    value: str = VALUE_SETTING,  # noqa: WPS110
) -> None:
    """Set config options.

    :param option: Can be database for the url, or debug_level
    :param value: The database url for engine (SQLModel)
    """
    if option == DATABASE:
        config_reader.set_database(value)

    if option == DEBUG_LEVEL:
        config_reader.set_debug_level(value)
