"""Extract table from a database."""

import json

from sqlalchemy import create_engine
from typer import Argument

from cliba.cli.app import app
from cliba.database.engine import create_init_engine
from cliba.migrate import export_table_to_yaml, import_transactions_from_yaml

ARGUMENT_DB_URL = Argument(..., help="Database url")
ARGUMENT_TABLE = Argument(..., help="Table name")
ARGUMENT_OUT_PATH = Argument(..., help="Path to output file")
ARGUMENT_IN_PATH = Argument(..., help="Path to input file")
ARGUMENT_OVERWRITES = Argument(..., help="JSON dict with overwrites in YAML")


@app.command()
def export_db_table(
    db_url: str = ARGUMENT_DB_URL,
    table: str = ARGUMENT_TABLE,
    out_path: str = ARGUMENT_OUT_PATH,
) -> None:
    """Extract a table from a database.

    This is a tool to extract a table of ANY database, supported by sqlalchemy,
    to a yaml file.

    :param db_url: Url of the database
                   e.g. postgresql+pg8000://user:password@localhost:port/database
    :param table: Table name
    :param out_path: File to write the output to
    """
    engine = create_engine(db_url)
    export_table_to_yaml(engine, table, out_path)


@app.command()
def import_transactions(
    data_path: str = ARGUMENT_IN_PATH, overwrites: str = ARGUMENT_OVERWRITES
) -> None:
    """Import transactions from yaml.

    :param data_path: Yaml file containing list with dicts of rows
    :param overwrites: JSON dictionary to overwrite fields in yaml rows
    :raises ValueError: If overwrites is not a JSON dictionary.
    """
    engine = create_init_engine()
    overwrites = json.loads(overwrites)
    if not isinstance(overwrites, dict):
        raise ValueError("Overwrites is not a dict!")

    import_transactions_from_yaml(engine, data_path, overwrites)
