"""Define reusable application arguments."""

from typing import List

import typer

from cliba.config.config_reader import find_config_file, read_config


class Completions(object):
    """Implement completion methods."""

    @staticmethod
    def alias() -> List[str]:  # noqa: WPS605
        """Complete alias argument.

        :returns: List of existing aliases
        """
        config = read_config(find_config_file())
        return list(config.accounts.keys())


class Arguments(object):
    """Implement Arguments for typer."""

    alias = typer.Argument(
        ...,
        help="The alias for the account",
        autocompletion=Completions.alias,
    )


class Options(object):
    """Implement Options for typer."""
