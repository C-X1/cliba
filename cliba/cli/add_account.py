"""Add account."""

import sys

import structlog
import typer

from cliba.cli.app import app
from cliba.cli.args import Arguments
from cliba.config import config_reader

logger = structlog.get_logger()

ARGUMENT_BANK_IDENTIFIER = typer.Argument(..., help="Bank identifier on the server")
ARGUMENT_USER_NAME = typer.Argument(..., help="Login user name")
ARGUMENT_SERVER = typer.Argument(..., help="Url of the Server")
OPTION_CUSTOMER_ID = typer.Option(
    default=None, help="Customer id, if required for the server"
)
OPTION_FORCE = typer.Option(
    default=False, help="Force overwrite of account alias if it exists"
)


@app.command()
def add_account(  # noqa: WPS211
    alias: str = Arguments.alias,
    bank_identifier: str = ARGUMENT_BANK_IDENTIFIER,
    user_id: str = ARGUMENT_USER_NAME,
    server_url: str = ARGUMENT_SERVER,
    customer_id: str = OPTION_CUSTOMER_ID,
    force: bool = OPTION_FORCE,
) -> None:
    """Add account.

    :param alias: The alias for the account
    :param bank_identifier: The identifier of the bank
    :param customer_id: The customer id (optional)
    :param user_id: The username you got from your bank.
    :param server_url: The server url for the fints endpoint.
                       Example:
                          https://hbci-pintan.gad.de/cgi-bin/hbciservlet (VR Banks)
    :param force: Force overwriting existing entries
    """
    try:
        config_reader.add_account(
            alias, bank_identifier, user_id, server_url, customer_id, force
        )
    except KeyError:
        logger.error("Alias {alias} exists, use --force to overwrite.")
        sys.exit(1)
