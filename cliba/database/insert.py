"""Insert new data."""

from typing import List

import structlog
from sqlalchemy.exc import IntegrityError
from sqlalchemy.future.engine import Engine
from sqlmodel import Session, SQLModel

logger = structlog.get_logger()


def insert(
    engine: Engine,
    datasets: List[SQLModel],
) -> None:
    """Insert datasets into database.

    :param engine: The database engine
    :param datasets: The datasets to add
    :raises IntegrityError: if an IntegrityError does not
                            originate from UNIQUE contraint
    """
    with Session(engine) as session:
        for dataset in datasets:
            dataset_type = type(dataset)
            exists = False
            session.add(dataset)
            try:
                session.commit()
            except IntegrityError as exception:
                if "unique" in str(exception).lower():
                    session.rollback()
                    exists = True
                else:
                    raise exception

            if exists:
                logger.debug(f"{dataset_type} already exists:\n{dataset}\n")
            else:
                logger.info(f"Inserted new {dataset_type}:\n{dataset}\n")
