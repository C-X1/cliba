"""Define schema of balance table."""

import datetime as dt
from typing import Optional

from pydantic import validator
from sqlmodel import Field, SQLModel

from cliba.database.schema.decimal_type import decimal_type
from cliba.database.validators import validate_iban


class Balance(SQLModel, table=True):
    """Specify balance table."""

    balance_id: Optional[int] = Field(default=None, primary_key=True)
    account_iban: str = Field(nullable=False)
    amount: decimal_type = Field(default=0, nullable=False)  # type: ignore
    currency: str = Field(nullable=False)
    date: dt.datetime = Field(nullable=False)

    _validate_iban = validator("account_iban", allow_reuse=True)(validate_iban)
