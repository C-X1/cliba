"""Store amount_type."""

from pydantic import condecimal

decimal_type = condecimal(max_digits=20, decimal_places=3)  # noqa: WPS432
