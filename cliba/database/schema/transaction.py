"""Describe Transaction Model."""

import datetime as dt
from types import MappingProxyType
from typing import Dict

from pydantic import validator
from sqlalchemy import Column, PrimaryKeyConstraint
from sqlmodel import JSON, Field, SQLModel

from cliba.database.schema.decimal_type import decimal_type
from cliba.database.validators import validate_iban

TRANSACTION_FINTS_DEFAULT_MAPPING = MappingProxyType(
    {
        "applicant_bic": "applicant_bin",
        "amount": "amount.amount",
    }
)


class Transaction(SQLModel, table=True):
    """Store FinTS transaction data."""

    __table_args__ = (
        PrimaryKeyConstraint(  # noqa: WPS604
            "account_iban",
            "currency",
            "date",
            "amount",
            "applicant_iban",
            "purpose",
            name="UniqueTransaction",
        ),
    )

    account_iban: str = Field(nullable=False)
    raw_data: Dict[str, str] = Field(nullable=False, sa_column=Column(JSON))

    # Interesting data from request
    date: dt.date = Field(nullable=False)
    applicant_name: str = Field(nullable=True)
    applicant_iban: str = Field(nullable=True)
    applicant_bic: str = Field(nullable=True)
    purpose: str = Field(nullable=False)
    amount: decimal_type = Field(default=0, nullable=False)  # type: ignore
    currency: str = Field(nullable=False)

    # validators
    _validate_iban = validator("applicant_iban", allow_reuse=True)(validate_iban)
    _validate_iban = validator("account_iban", allow_reuse=True)(validate_iban)
