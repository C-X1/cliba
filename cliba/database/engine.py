"""Create the database engine."""

from typing import Union

from sqlalchemy.future.engine import Engine
from sqlmodel import SQLModel, create_engine

from cliba.config.config_reader import find_config_file, read_config
from cliba.database.schema.balance import Balance  # noqa: F401
from cliba.database.schema.transaction import Transaction  # noqa: F401


def create_init_engine(url: Union[str, None] = None) -> Engine:
    """Initialize database if needed and return engine.

    :param url: overwrite default url from settings
    :returns: engine
    """
    config = read_config(find_config_file())
    engine = create_engine(url or config.database_url or "", echo=True)
    SQLModel.metadata.create_all(engine)
    return engine
