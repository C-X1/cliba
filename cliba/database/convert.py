"""Convert FinTS types to database."""

from copy import deepcopy
from datetime import datetime

from fints.models import SEPAAccount  # type: ignore
from mt940.models import Amount as mtAmount  # type: ignore
from mt940.models import Balance as mtBalance  # type: ignore
from mt940.models import Transaction as mtTransaction  # type: ignore

from cliba.database.schema.balance import Balance
from cliba.database.schema.transaction import (
    TRANSACTION_FINTS_DEFAULT_MAPPING,
    Transaction,
)


def transaction_dict_to_string(transaction_data: dict) -> None:
    """Convert data to strings.

    :param transaction_data: Copy from mtTransaction.data
    """
    for key, entry in list(transaction_data.items()):
        if isinstance(entry, mtAmount):
            transaction_data.pop(key)
            transaction_data[f"{key}.amount"] = str(entry.amount)
            transaction_data[f"{key}.currency"] = str(entry.currency)
        else:
            transaction_data[key] = str(entry)


def prepare_transaction_dict(transaction_data: dict) -> dict:
    """Prepare transaction dictionary.

    :param transaction_data: mtTransactionData
    :returns: Dict with data for Transaction class for database
    """
    init_dict = {}
    fields_to_ignore = ["account_iban", "raw_data"]

    for key in Transaction.__fields__.keys() - fields_to_ignore:
        from_key = TRANSACTION_FINTS_DEFAULT_MAPPING.get(key, key)
        init_dict[key] = transaction_data.get(from_key)

    return init_dict


def to_database_transaction(
    account: SEPAAccount, transaction: mtTransaction
) -> Transaction:
    """Convert a mt940 transaction to the database type.

    :param account: The account for the transaction
    :param transaction: The transaction
    :returns: Transaction for database
    """
    transaction_raw = deepcopy(transaction.data)

    transaction_dict_to_string(transaction_raw)

    transaction_dict = prepare_transaction_dict(deepcopy(transaction_raw))

    transaction_dict["raw_data"] = transaction_raw
    transaction_dict["account_iban"] = account.iban

    return Transaction(**transaction_dict)


def to_database_balance(account: SEPAAccount, balance: mtBalance) -> Balance:
    """Convert a mt940 balance to the database type.

    :param account: The account for the balance
    :param balance: The balance
    :returns: Balance for database
    """
    return Balance(
        account_iban=account.iban,
        amount=balance.amount.amount,
        currency=balance.amount.currency,
        date=datetime.now(),
    )
