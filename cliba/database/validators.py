"""Implement validators for pydantic."""

from typing import Union

import schwifty


def validate_iban(iban: Union[None, str]) -> Union[None, str]:
    """Validate an IBAN number.

    :param iban: The IBAN number to validate
    :returns: IBAN
    """
    if iban:
        schwifty.IBAN(iban=iban)  # Will raise an exception on failure.
    return iban
