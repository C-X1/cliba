"""Implement synchronizing Accounts."""

from typing import List

from fints.client import FinTS3Client  # type: ignore
from fints.models import SEPAAccount  # type: ignore
from fints.utils import minimal_interactive_cli_bootstrap  # type: ignore
from sqlalchemy.future.engine import Engine
from sqlmodel import SQLModel

from cliba.config.config_reader import find_config_file, read_config
from cliba.database import convert
from cliba.database.engine import create_init_engine
from cliba.database.insert import insert
from cliba.fints_client import create_fints_client
from cliba.tan_guard import guard_tan_request


def sync_accounts(alias: str) -> None:
    """Sync accounts of login.

    :param alias: The alias of the account in the config
    """
    account_config = read_config(find_config_file()).accounts[alias]
    engine = create_init_engine()
    client = create_fints_client(alias, **account_config.dict())
    minimal_interactive_cli_bootstrap(client)

    with client:
        accounts = guard_tan_request(client.get_sepa_accounts)
        for account in accounts:
            process_account(engine, client, account)


def process_account(engine: Engine, client: FinTS3Client, account: SEPAAccount) -> None:
    """Process account synchronization and store data.

    :param engine: The database engine
    :param client: The FinTS client
    :param account: The account to process
    """
    balance = guard_tan_request(client.get_balance, account=account)
    transactions = guard_tan_request(client.get_transactions, account=account)

    database_insertions: List[SQLModel] = []

    database_insertions.append(convert.to_database_balance(account, balance))

    for transaction in transactions:
        database_insertions.append(convert.to_database_transaction(account, transaction))

    insert(engine, database_insertions)
