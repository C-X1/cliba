"""Implement Settings and helpers."""

import os
import shutil
import sys

import structlog

from cliba.file_helpers import create_dir

logger = structlog.get_logger()

CLIBA_DIR = os.path.join(os.environ["HOME"], ".cliba")
SERVER_FILE_NAME = "server"
DATABASE_URL_FILE_NAME = "database"
DATABASE_URL_FILE_PATH = os.path.join(
    CLIBA_DIR, os.path.join(CLIBA_DIR, DATABASE_URL_FILE_NAME)
)


def account_dir(bank_id: str, user_name: str) -> str:
    """Create bank account.

    :param bank_id: Code of the bank
    :param user_name: Username for the account
                      (received with your online account)
    :returns: Account directory path
    """
    return os.path.join(CLIBA_DIR, bank_id, user_name)


def create_account(bank_id: str, user_name: str, server_url: str) -> None:
    """Create bank account.

    :param bank_id: Code of the bank
    :param user_name: Username for the account
                      (received with your online account)
    :param server_url: The URL for the fints server
    """
    path = account_dir(bank_id, user_name)
    create_dir(path)

    with open(os.path.join(path, SERVER_FILE_NAME), "w") as server_file:
        server_file.write(server_url)


def remove_account(bank_id: str, user_name: str) -> None:
    """Remove bank account.

    :param bank_id: Code of the bank
    :param user_name: Username for the account
                      (received with your online account)
    :raises RuntimeError: If settings path does not exist or is not a directory.
    """
    directory = account_dir(bank_id, user_name)

    if not os.path.isdir(directory):
        raise RuntimeError(f"Path {directory} does not exist, or is not a directory.")

    shutil.rmtree(directory)


def server_url(bank_id: str, user_name: str) -> str:
    """Retrieve server url from settings.

    :param bank_id: Code of the bank
    :param user_name: Username for the account
                      (received with your online account)
    :returns: Server URL
    """
    with open(
        os.path.join(account_dir(bank_id, user_name), SERVER_FILE_NAME)
    ) as server_file:
        return server_file.read()


def database_url() -> str:
    """Get database url.

    :returns: Database url
    """
    try:
        with open(DATABASE_URL_FILE_PATH, "r") as url_file:
            return url_file.read()
    except FileNotFoundError:
        logger.fatal("Database URL not set.")
        sys.exit(1)
