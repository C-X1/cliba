"""Make cliba executable by `python -m cliba`."""
from cliba import cliba

if __name__ == "__main__":  # pragma: no cover
    cliba.main()
